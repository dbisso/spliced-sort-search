<?php
/*
Plugin Name: Spliced Sort Search
Plugin URI: http://spliced.co
Description: Sorts search results by post type
Version: 0.1
Author: Spliced Digital
Author URI: http://spliced.co
*/

/**
 * Copyright (c) 2013 Spliced. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */

add_action( 'admin_init', 'spliced_sort_search_version_check' );
register_activation_hook( __FILE__, 'spliced_sort_search_version_check' );

function spliced_sort_search_version_check() {
	$min_php = '5.3.5';

	if ( version_compare( PHP_VERSION, $min_php, '<' ) ) {
		$plugin = plugin_basename( __FILE__ );
		$message = sprintf(
			'This plugin (%s) requires at least PHP %s. You are currently running PHP %s. <button type="button" class="button" href="%s">Disable the plugin</button>',
			$plugin,
			$min_php,
			PHP_VERSION,
			remove_query_arg( 'action' )
		);

		deactivate_plugins( $plugin );
		unset( $_GET['activate'] );
		unset( $_GET['plugin'] );

		wp_die( $message );
	} else {
		include_once __DIR__ . '/bootstrap.php';
	}
}

include_once __DIR__ . '/bootstrap.php';

<?php
namespace Spliced\Plugin\SortSearch;

/**
 * Class Spliced\Plugin\SortSearch\Frontend
 */
class Frontend {
	static $hooker;

	public function bootstrap( $hooker = null ) {
		if ( !$hooker || !method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}

	public function filter_posts_orderby( $orderby, $query ) {
		global $wpdb;

		// We need to use DESC on the orderby clause so that undefined
		// post types appear last, but that flips the sort order of the
		// FIELD clause so we have to reverse our array to undo that effect.
		$order_set = array_reverse( apply_filters( 'spliced_sort_serch_post_types', array() ) );

		if ( ! empty( $order_set ) && $query->is_search ) {
			$order = implode( "', '", array_map( 'esc_sql', $order_set ) );
			$orderby = "FIELD({$wpdb->posts}.post_type, '$order') DESC, " . $orderby;
		}

		return $orderby;
	}

	function get_next_post_type() {
		static $current = '';
		$post = get_post();

		$post_type = $post->post_type;

		if ( empty( $current ) || $current !== $post_type ) {
			$current = $post_type;
			return $post_type;
		}

		if ( $current === $post_type ) {
			return false;
		}
	}
}